package backend;

import com.sun.javafx.collections.MappingChange;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import javax.swing.*;
import java.io.*;
import java.util.*;

import static backend.ColumnSetter.setRows;
import static backend.Constant.EMPTY;

public interface Testing {

    static List TextFile(DefaultListModel<String> listModel, ArrayList<String> code) throws IOException {
        String st;
//            StringBuilder tmp = new StringBuilder();
        String tmp = "";
        ArrayList<String> tmpList = new ArrayList<>();
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            File file = new File(listModel.get(i));
            BufferedReader br = new BufferedReader(new FileReader(file));
            int j = 0;
            while ((st = br.readLine()) != null) {
                if (!st.isEmpty()) {
//                    tmp.append(st).append("\n");
                    tmp += st +"\n";
//                    for (String s : code) {
//
//                        if (st.contains(s)) {
//                            System.out.println(tmp);
//                        }
//                    }
                }else {
//                    System.out.println(tmp);
                    if (!tmp.isEmpty()){
                        tmpList.add(j,tmp);
                        j++;
                    }
                    tmp ="";
                }

//                System.out.println(tmp2);
            }
//            System.out.println(tmpList);
        }
//        for (String value : tmpList) {
//            for (String s : code) {
        for (String s : code) {
            for (String value : tmpList) {
                if (value.contains(s)) {
                    System.out.println(value);
                    result.add(value);
                }
            }
        }
        return result;
    }

    static ArrayList<String> odsFile(String path) {
        ArrayList<String> code = new ArrayList<String>();

        File file = new File(path);

        SpreadSheet spreadsheet;

        try {
            //Getting the 0th sheet for manipulation| pass sheet name as string

            spreadsheet = SpreadSheet.createFromFile(file);

            //Get row count and column count
            int nColCount = spreadsheet.getSheet(0).getColumnCount();
            int nRowCount = spreadsheet.getSheet(0).getRowCount();

//            System.out.println("Rows :"+nRowCount);
//            System.out.println("Cols :"+nColCount);
            //Iterating through each row of the selected sheet
            MutableCell cell = null;
//            for (int nRowIndex = 7; nRowIndex < 30; nRowIndex++) {
//                //Iterating through each column
//                for (int nColIndex = 2; nColIndex < 3; nColIndex++) {
//                    cell = spreadsheet.getSheet(0).getCellAt(nColIndex, nRowIndex);
////                    System.out.print(cell.getValue() + " ");
//                    code.add(String.valueOf(cell.getValue()));
//                    if (cell.getValue().equals("")) {
//                        code.remove(code.size() -1);
//                        return code;
//                    }
//                }
////                System.out.println();
//                if (cell.getValue().equals("")) {
//                    code.remove(code.size() -1);
//                    return code;
//                }
//            }
            int nRowIndex = 7;
            while (true){
                for (int nColIndex = 2; nColIndex < 3; nColIndex++) {
                    cell = spreadsheet.getSheet(0).getCellAt(nColIndex, nRowIndex);
//                    System.out.print(cell.getValue() + " ");
                    code.add(String.valueOf(cell.getValue()));
                    if (cell.getValue().equals("")) {
                        code.remove(code.size() -1);
                        return code;
                    }
                }
//                System.out.println();
                if (cell.getValue().equals("")) {
                    code.remove(code.size() -1);
                    return code;
                }
                nRowIndex++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return code;
    }
    static void excelFile(String path, List result) throws IOException, InvalidFormatException {
        //Workbook
        Workbook workbook = WorkbookFactory.create(new File(path));

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
//        data.put("1", new Object[] {"value", "test"});
//        int a = 2;
////        for (Object o : result) {
////            data.put(Integer.toString(a), new Object[]{"",o});
////            a++;
////        }
        setRows(result, data);
//        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
//        data.put("2", new Object[] {1, "Amit", "Shukla"});
//        data.put("3", new Object[] {2, "Lokesh", "Gupta"});
//        data.put("4", new Object[] {3, "John", "Adwards"});
//        data.put("5", new Object[] {4, "Brian", "Schultz"});

        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        List<String> sortedList = new ArrayList<String>(keyset);
        Collections.sort(sortedList);
        int rownum = 1;
//        for (String key : keyset) {
        for (int i =3; data.size() + 2 >= i; i++) {
            Row row = sheet.createRow(rownum++);
//            Object [] objArr = data.get(key);
            Object [] objArr = data.get(Integer.toString(i));
            int cellnum = 0;
            for (Object obj : objArr)
            {
                Cell cell = row.createCell(cellnum++);
                if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(System.getProperty("user.home") + "\\Desktop\\testNew.xls"));
            workbook.write(out);
            out.close();
            System.out.println("testNew.xls written successfully on disk.");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}


