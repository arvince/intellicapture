package backend;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static backend.Constant.EMPTY;

class ColumnSetter {
    static void setRows(List result, Map<String, Object[]> data) {
        int a = 3;
        for (Object o : result) {
            data.put(Integer.toString(a), new Object[]{EMPTY, EMPTY, EMPTY, EMPTY, remitterLastName(o), trandate(o), beneId(o), EMPTY, EMPTY, beneficiaryLastName(o), transactionReferenceNumber(o), fundingCurrency(o), fundingAmount(o), principalCurrency(o), EMPTY, EMPTY, EMPTY, settlementCurrency(o), settlementAmount(o), settlementMode(o), beneficiaryAddressNoStreet(o), EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, beneficiaryBankNote(o), bpiBranchCode(o), beneBankAcctNo(o), beneCustomerType(o), alternateBeneficiary(o), EMPTY, EMPTY, EMPTY, EMPTY, remitterMessageToBenef(o), EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, beneEmailAddress(o), EMPTY, EMPTY, EMPTY, remitterAddressNumberAndStreet(o), EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, remitterCustomerType(o), EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, receiverCorrBank(o), senderCorrBank(o), sendingCorrBank(o), receivingBank(o), modeChrg(o)});
            a++;
        }
    }

    static String remitterLastName(Object object) {
        String word = object.toString();
        if (!word.contains("Ordering Customer")){
            return "";
        }
        int x = word.indexOf("Ordering Customer") + 27;
        int y = x + word.substring(x).indexOf("\n");
        y = y + word.substring(y + 1).indexOf("\n");
        return word.substring(x, y).replace("\n", "").trim().replaceAll("( )+", " ");
    }

    static String trandate(Object object) {
        String word = object.toString();
        if (!word.contains("Settlement Amount")){
            return "";
        }
        int x = word.indexOf("Settlement Amount") + 32;
        int y = x + word.substring(x).indexOf("Currency");
        return word.substring(x + 3, y).trim() + "20" + word.substring(x, y - 5).trim();
    }

    static String beneId(Object object) {
        String word = object.toString();
        if (!word.contains("Beneficiary Customer")){
            return "";
        }
        int x = word.indexOf("Beneficiary Customer") + 29;
        int y = x + word.substring(x).indexOf("\n");
        return word.substring(x, y).trim().replace("-", "");
    }

    static String beneficiaryLastName(Object object) {
        String word = object.toString();
        if (!word.contains("Beneficiary Customer*59")){
            return "";
        }
        int x = word.indexOf("Beneficiary Customer*59") + 17;
        x = x + word.substring(x).indexOf("\n");
        int y = x + word.substring(x + 1).indexOf("\n");
        return word.substring(x, y).replace("\n", "").trim().replaceAll("( )+", " ");
    }

    static String transactionReferenceNumber(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 27;
        int y = x + word.substring(x).indexOf("\n");
        return word.substring(x, y).trim();
    }

    static String fundingCurrency(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 25;
        x = x + word.substring(x).indexOf("Currency ") + 9;
        int y = x + word.substring(x + 1).indexOf("Amount ");
        return word.substring(x, y).trim();
    }

    static String fundingAmount(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 32;
        x = x + word.substring(x).indexOf("Amount ") + 16;
        x = x + word.substring(x).indexOf("Amount ") + 6;
        int y = x + word.substring(x + 1).indexOf("\n");
        return word.substring(x, y).trim().replace(".", "").replace(",", ".");
    }

    static String principalCurrency(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 17;
        x = x + word.substring(x).indexOf("Currency ") + 8;
        int y = x + word.substring(x + 1).indexOf("Amount ");
        return word.substring(x, y).trim();
    }

    static String settlementCurrency(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 25;
        x = x + word.substring(x).indexOf("Currency ") + 9;
        int y = x + word.substring(x + 1).indexOf("Amount ");
        return word.substring(x, y).trim();
    }

    static String settlementAmount(Object object) {
        String word = object.toString();
        if (!word.contains("Sender's Reference  *20")){
            return "";
        }
        int x = word.indexOf("Sender's Reference  *20") + 32;
        x = x + word.substring(x).indexOf("Amount ") + 16;
        x = x + word.substring(x).indexOf("Amount ") + 6;
        int y = x + word.substring(x + 1).indexOf("\n");
        return word.substring(x, y).trim().replace(".", "").replace(",", ".");
    }

    static String settlementMode(Object object) {
//        if (beneficiaryBankNote(object).isEmpty() && bpiBranchCode(object).isEmpty()){
//            return "01";
//        }else {
//            return "03";
//        }
        return "01";
    }

    static String beneficiaryAddressNoStreet(Object object) {
        String word = object.toString();
        if (!word.contains("Beneficiary Customer*59")){
            return "";
        }
        int x = word.indexOf("Beneficiary Customer*59") + 17;
        x = x + word.substring(x + 1).indexOf("\n");
        x = x + word.substring(x + 1).indexOf("\n");
        if (!word.substring(x).contains("Remittance Info.")){
            return "";
        }
        int y = x + word.substring(x).indexOf("Remittance Info.");
        return word.substring(x, y).replace("\n", "").trim().replaceAll("( )+", " ");
    }

    static String beneficiaryBankNote(Object object) {
        String word = object.toString();
        int x = word.indexOf("Account with Inst.   57");
        if (x < 0) {
            return "";
        }
        int y = x + word.substring(x + 1).indexOf("Beneficiary Customer*59");
        return word.substring(x + 27, y).trim().replace("\n", "").replace("  ", "");
    }

    static String bpiBranchCode(Object object) {
        String word = object.toString();
        int x = word.indexOf("Account with Inst.   56");
        if (x < 0) {
            return "";
        }
        int y = x + word.substring(x + 1).indexOf("\n");
        return word.substring(x, y).trim();
    }

    static String beneBankAcctNo(Object object) {
        String word = object.toString();
        if (!word.contains("Beneficiary Customer*59")){
            return "";
        }
        int x = word.indexOf("Beneficiary Customer*59 ") + 29;
        int y = x + word.substring(x + 1).indexOf("\n");
        return word.substring(x, y).trim().replace("-", "");
    }

    static String beneCustomerType(Object object) {
        String word = beneficiaryLastName(object);
//        if (word.contains("CORP") || word.contains("COMPANY") || word.contains("IN") || word.contains("LLC")) {
//            return "C";
//        } else if (word.contains("IND")) {
//            return "I";
//        } else {
//            return "I";
//        }
        return "C";
    }

    static String alternateBeneficiary(Object object) {
        String word = object.toString();
        if (!word.contains("End to End Ref. 121:")){
            return "";
        }
        int x = word.indexOf("End to End Ref. 121:");
        int y = x + word.substring(x).indexOf("\n") + 1;
        return word.substring(x, y).trim().replace("End to End Ref. 121: ", "").replaceFirst("-", "");
    }

    static String remitterMessageToBenef(Object object) {
        String word = object.toString();
        if (!word.contains("Remittance Info.     70   :")){
            return "";
        }
        int x = word.indexOf("Remittance Info.     70   :") + 23;
        int y = x + word.substring(x).indexOf("Details of Charges  *71 A :");
        return word.substring(x, y).trim().replace("End to End Ref. 121: ", "").replaceFirst("-", "").trim().replaceAll(" +", " ").replaceAll("\r*\n*", "");
    }

    static String beneEmailAddress(Object object) {
        try{
        String word = object.toString();
        ArrayList<String> emails = new ArrayList<String>();
        String result = "";
        int x = word.indexOf("Sender's Charges     71 F");
        if (x < 0) {
            return "";
        }
        int y = x + word.substring(x).indexOf("\n");
        int x2 =0;
        int y2 =0;
        emails.add(word.substring(x, y).trim());
        while (word.substring(y).indexOf("Sender's Charges     71 F") > 0){
            x2 = word.substring(y+y2).indexOf("Sender's Charges     71 F") + y+y2;
            y2 = x2 + word.substring(x2).indexOf("\n");
            y = y2;
            emails.add(word.substring(x2, y2).trim());
        };
        for (String email : emails) {
            result = result.concat(email.replace("Sender's Charges     71 F : Currency", "").replace("Amount", "").replace(",", "."));
        }
        if (result.isEmpty()) {
            return "USD 0.0";
        }
        return result.trim();
        } catch (Exception e){
            return "";
        }
    }

    static String remitterAddressNumberAndStreet(Object object) {
        String word = object.toString();
        int x = word.indexOf("Ordering Customer   *50 ");
        if (x < 0) {
            return "";
        }
        x = x + word.substring(x + 1).indexOf("\n");
        x = x + word.substring(x + 1).indexOf("\n");
        int y = x + word.substring(x).indexOf("\n");
        y = y + word.substring(y + 1).indexOf("\n");
        return word.substring(x, y).trim();
    }

    static String remitterCustomerType(Object object) {
        String word = remitterLastName(object);
//        if (word.contains("CORP") || word.contains("COMPANY") || word.contains("IN") || word.contains("LLC")) {
//            return "C";
//        } else if (word.contains("IND")) {
//            return "I";
//        } else {
//            return "I";
//        }
        return "C";
    }

    static String receiverCorrBank(Object object) {
        String word = object.toString();
        int x = word.indexOf("Receiver's Corr.");
        if (x < 0) {
            return "";
        }
        int y = x + word.substring(x).indexOf("\n");
        return !word.substring(x).contains("54 A") ? "" : word.substring(x, y).trim().replace(" ", "").replace("Receiver'sCorr.54A:", "").substring(0, 8) + "" + fundingCurrency(object);
    }

    static String senderCorrBank(Object object) {
        String word = object.toString();
        int x = word.indexOf("Sender's Corr.");
        if (x < 0) {
            return "";
        }
        int y = x + word.substring(x + 1).indexOf("\n");
        return !word.substring(x).contains("53 A") ? "" : word.substring(x, y).trim().replace(" ", "").replace("Sender'sCorr.53A:", "").substring(0, 8) + "" + fundingCurrency(object);
    }

    static String sendingCorrBank(Object object) {
        String word = object.toString();
        if (!word.contains("Application Header")){
            return "";
        }
        int x = word.indexOf("Application Header");
        int y = x + word.substring(x).indexOf("\n");
        return word.substring(x + 38, y).trim().substring(0, 8) + "" + fundingCurrency(object);
    }

    static String receivingBank(Object object) {
//        int x = word.indexOf("Beneficiary Customer*59 ") + 17;
//        int y = x + word.substring(x + 1).indexOf("\n");
        return "";
    }

    static String modeChrg(Object object) {
        String word = object.toString();
        if (!word.contains("Details of Charges  *71 A :")){
            return "";
        }
        int x = word.indexOf("Details of Charges  *71 A :");
        int y = x + word.substring(x).indexOf("\n") + 1;
        return word.substring(x, y).trim().replace("Details of Charges  *71 A : ", "");
    }
}
