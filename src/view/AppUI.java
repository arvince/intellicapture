package view;

import backend.JTableTest;
import backend.TableBean;
import backend.Testing;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jopendocument.dom.template.engine.DataModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class AppUI {
    private JButton button1;
    private JPanel panel1;
    private JTextField textField1;
    private JButton button2;
    private JList<String> list1;
    private JTextArea textArea1;
    private JButton button3;
    private JButton button4;
    private JTextField textField2;
    private JTextField textField3;
    private JButton button5;
    private JButton clearButton;
    private DefaultListModel<String> listModel = new DefaultListModel<>();
    private JFileChooser chooser = new JFileChooser();
    private String odsPath;
    private String excelPath;

    private AppUI() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Hello World");

                JFrame frame = new JFrame("JTable Test");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                JTable table = createTable();
                JScrollPane scrollPane = new JScrollPane(table);
                frame.getContentPane().add(scrollPane);
                frame.pack();
                frame.setVisible(true);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButton2ActionPerformed(e);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jButton3ActionPerformed(e);
                } catch (IOException | InvalidFormatException ex) {
                    ex.printStackTrace();
                }
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jButton4ActionPerformed(e);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jButton5ActionPerformed(e);
                } catch (IOException | InvalidFormatException ex) {
                    ex.printStackTrace();
                }
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jButtonClearActionPerformed(e);
                } catch (IOException | InvalidFormatException ex) {
                    ex.printStackTrace();
                }
            }
        });

//    TableBean test = new TableBean();
//
//    test.setFundingCurrency("A");
//    test.setAccountNumber("B");
//    test.setSettlementCurrency("C");
//    test.setAmount("D");
//
//    test.setFundingCurrency("E");
//
//    table1.setModel(test);
//    table1.isVisible();

//        table1 = createTable();

    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        String filename = file.getAbsolutePath();
        textField1.setText(filename);

        listModel.addElement(filename);
        list1.setModel(listModel);
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvalidFormatException {
        Testing.excelFile(getExcelPath(), Testing.TextFile(listModel, Testing.odsFile(getOdsPath())));
        textArea1.setText("Success");
    }

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        String filename = file.getAbsolutePath();
        textField2.setText(filename);

        setOdsPath(filename);
    }

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvalidFormatException {
        chooser.showOpenDialog(null);
        File file = chooser.getSelectedFile();
        String filename = file.getAbsolutePath();
        textField3.setText(filename);

        setExcelPath(filename);
    }

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvalidFormatException {
        if(!textField1.getText().isEmpty()){
            textField1.setText(null);
        }
        if(!textField2.getText().isEmpty()){
            textField2.setText(null);
        }
        if(!textField3.getText().isEmpty()){
            textField3.setText(null);
        }
        if (listModel.getSize() != 0){
            list1.removeAll();
            listModel.removeAllElements();
        }
    }

    public static void main(String[] args) {
//        JTable table = createTable();
//        JScrollPane scrollPane = new JScrollPane(table);
        JFrame jframe = new JFrame("intelliCAPTURE");
//        jframe.getContentPane().add(scrollPane);

        jframe.setContentPane(new AppUI().panel1);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.pack();
        jframe.setVisible(true);
    }

    private String getOdsPath() {
        return odsPath;
    }

    private void setOdsPath(String odsPath) {
        this.odsPath = odsPath;
    }

    public String getExcelPath() {
        return excelPath;
    }

    private void setExcelPath(String excelPath) {
        this.excelPath = excelPath;
    }

    public static JTable createTable(){
        String[] columnNames = {"First Name", "Last Name"};
        Object[][] data = {{"Kathy", "Smith"},{"John", "Doe"}};
        JTable table = new JTable(data, columnNames);
        table.setFillsViewportHeight(true);

        return table;
    }
}
